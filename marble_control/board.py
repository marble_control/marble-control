"""Interface for boards."""
from dataclasses import dataclass
import functools
import random
import pymunk
import pymunk.matplotlib_util
import matplotlib.pyplot as plt
import math
import numpy as np
from .polygon import ConvexPolygon
from .marble import Marble
from .waypoint import Waypoint
from .pymunk_facade import add_marble
from .pymunk_facade import add_polygon
from .pymunk_facade import find_empty_space
from .pymunk_facade import find_empty_space


def _get_wall_vertices(
        size: tuple[float, float],
        wall_thickness: float
        ):
    wall_vertices = [
        [
            (-size[0]/2, size[1]/2),
            (size[0]/2, size[1]/2),
            (size[0]/2, size[1]/2-wall_thickness),
            (-size[0]/2, size[1]/2-wall_thickness),
        ], [
            (size[0]/2, size[1]/2),
            (size[0]/2, -size[1]/2),
            (size[0]/2-wall_thickness, -size[1]/2),
            (size[0]/2-wall_thickness, size[1]/2),
        ], [
            (size[0]/2, -size[1]/2),
            (-size[0]/2, -size[1]/2),
            (-size[0]/2, -size[1]/2+wall_thickness),
            (size[0]/2, -size[1]/2+wall_thickness),
        ], [
            (-size[0]/2, -size[1]/2),
            (-size[0]/2, size[1]/2),
            (-size[0]/2+wall_thickness, size[1]/2),
            (-size[0]/2+wall_thickness, -size[1]/2),
        ],
    ]
    return wall_vertices


waypoint_color = pymunk.space_debug_draw_options.SpaceDebugColor(
        r=159, g=255, b=15, a=255.0
    )

waypoint_velocity_color = pymunk.space_debug_draw_options.SpaceDebugColor(
        r=0, g=0, b=0, a=255.0
    )

marble_color = pymunk.space_debug_draw_options.SpaceDebugColor(
        r=247, g=15, b=255, a=255.0
    )

polygon_color = pymunk.space_debug_draw_options.SpaceDebugColor(
        r=80, g=80, b=80, a=255.0
    )


@dataclass
class Board:
    """A board with a controllable marble and polygonal obstacles."""
    marble: Marble
    obstacles: tuple[ConvexPolygon, ...]
    size: tuple[float, float]
    dt: float
    frame_skip: int
    waypoints: list[Waypoint]

    @staticmethod
    def random(
            marble_velocity: tuple[float, float] = (0, 0),
            marble_mass: float = 1.0,
            marble_radius: float = 1.0,
            marble_elasticity: float = 1.0,
            polygon_n: int = 10,
            vertex_n: int = 5,
            polygon_position_scale: float = 50.0,
            polygon_size_scale: float = 20.0,
            polygon_elasticity: float = 0.8,
            polygon_friction: float = 0.9,
            size: tuple[float, float] = (50.0, 50.0),
            dt: float = 1.0/60.0,
            frame_skip: int = 1,
            walls: bool = True,
            wall_thickness: float = 5.0,
            waypoint_n: int = 3,
            waypoint_radius: float = 1.0,
            waypoint_velocity_scale: float = 1.0,
            ) -> "Board":
        """Return a board with random obstacles and waypoints."""
        obstacles = [
                ConvexPolygon.random(
                    vertex_n,
                    polygon_size_scale,
                    polygon_position_scale,
                    elasticity=polygon_elasticity,
                    friction=polygon_friction,
                )
                for _ in range(polygon_n)
            ]
        if walls:
            obstacles += [
                    ConvexPolygon(
                        vertices=vertices,
                        elasticity=polygon_elasticity,
                        friction=polygon_friction,
                    )
                    for vertices in _get_wall_vertices(size, wall_thickness)
                ]

        # Find an empty space to place the marble
        space = pymunk.Space()
        shape_filter = pymunk.ShapeFilter()
        for polygon in obstacles:
            add_polygon(space, polygon)
        marble_position = find_empty_space(
                position_range=(size[0]/2, size[1]/2),
                radius=marble_radius,
                space=space,
                shape_filter=shape_filter,
            )

        # Find empty spaces to place the waypoints
        waypoints = list()
        for _ in range(waypoint_n):
            position = find_empty_space(
                    position_range=(size[0]/2, size[1]/2),
                    radius=waypoint_radius,
                    space=space,
                    shape_filter=shape_filter,
                )
            velocity = (
                    random.uniform(
                        -waypoint_velocity_scale,
                        waypoint_velocity_scale
                    ),
                    random.uniform(
                        -waypoint_velocity_scale,
                        waypoint_velocity_scale
                    )
                )
            waypoints.append(Waypoint(position, waypoint_radius, velocity))

        # Build the marble and the board
        marble = Marble(
                position=marble_position,
                velocity=marble_velocity,
                mass=marble_mass,
                radius=marble_radius,
                elasticity=marble_elasticity,
            )
        board = Board(
                marble=marble,
                obstacles=tuple(obstacles),
                size=size,
                dt=dt,
                frame_skip=frame_skip,
                waypoints=waypoints,
            )
        return board

    @functools.cached_property
    def pymunk_space(self):
        """The pymunk space for simulating dynamics. It is guaranteed that the
        first body in the space is the marble's body and the rest are
        obstacles. Likewise, the first shape in the space is the marble's shape
        and the rest are obstacles."""
        pymunk_space = pymunk.Space()

        # Add marble
        add_marble(pymunk_space, self.marble)

        # Add polygons
        for polygon in self.obstacles:
            add_polygon(pymunk_space, polygon)
        return pymunk_space

    @property
    def pymunk_marble_body(self):
        """The pymunk body for the board's marble."""
        return self.pymunk_space.bodies[0]

    @property
    def pymunk_obstacle_shapes(self):
        """The pymunk body for the board's marble."""
        return self.pymunk_space.shapes[1:]

    @property
    def pixel_array(self) -> list[list[int]]:
        """Pixels of the current board represented as a square image."""
        pixels = 600
        inches = 8
        dpi = pixels/inches
        fig, ax = plt.subplots(figsize=(inches, inches), dpi=dpi)
        ax.set_aspect("equal")
        options = pymunk.matplotlib_util.DrawOptions(ax)
        options.draw_dot(
                pos=self.marble.position,
                size=self.marble.radius,
                color=marble_color,
            )
        for polygon in filter(
                    lambda shape: isinstance(shape, pymunk.Poly),
                    self.pymunk_space.shapes
                ):
            options.draw_polygon(
                    verts=polygon.get_vertices(),
                    radius=0.0,
                    outline_color=polygon_color,
                    fill_color=polygon_color,
                )
        for i, waypoint in enumerate(self.waypoints):
            color = pymunk.space_debug_draw_options.SpaceDebugColor(
                r=waypoint_color.r,
                g=waypoint_color.g,
                b=waypoint_color.b,
                a=(1-(i+1)/(len(self.waypoints)+1))*255.0
            )
            waypoint_position_delta = (
                    waypoint.position[0]+waypoint.velocity[0],
                    waypoint.position[1]+waypoint.velocity[1],
                )
            options.draw_dot(
                pos=waypoint.position,
                size=waypoint.radius,
                color=color,
            )
            options.draw_segment(
                a=pymunk.Vec2d(
                    waypoint.position[0], waypoint.position[1]
                ),
                b=pymunk.Vec2d(
                    waypoint_position_delta[0], waypoint_position_delta[1]
                ),
                color=waypoint_velocity_color,
            )

        ax.set_xlim(left=-self.size[0]/2, right=self.size[0]/2)
        ax.set_ylim(bottom=-self.size[1]/2, top=self.size[1]/2)
        fig.canvas.draw()
        pixel_array = np.fromstring(fig.canvas.tostring_rgb(), dtype='uint8')
        pixel_array = pixel_array.reshape((pixels, pixels, 3))
        plt.close(fig)
        return pixel_array

    def apply_force_marble(self, force: tuple[float, float]):
        """Apply force on the marble."""
        self.pymunk_marble_body.apply_force_at_local_point(force)

    def step(self):
        """Step the simulation."""
        # Check if the marble's position or velocity was changed, if so, update
        # the simulator's state.
        if self.marble.position != self.pymunk_marble_body.position:
            self.pymunk_marble_body.position = self.marble.position
        if self.marble.velocity != self.pymunk_marble_body.velocity:
            self.pymunk_marble_body.velocity = self.marble.velocity

        # Step simulation
        for _ in range(self.frame_skip):
            self.pymunk_space.step(self.dt)

        # Update the marble
        marble = Marble(
                tuple(self.pymunk_marble_body.position),
                tuple(self.pymunk_marble_body.velocity),
                self.marble.mass,
                self.marble.radius,
                self.marble.elasticity,
            )
        self.marble = marble

        # Check if we arrived at the waypoint
        if len(self.waypoints) > 0:
            distance_to_waypoint = math.sqrt(sum(
                    (xi-yi)**2
                    for (xi, yi)
                    in zip(self.marble.position, self.waypoints[0].position)
                ))
            if distance_to_waypoint <= self.waypoints[0].radius:
                self.waypoints.pop(0)
