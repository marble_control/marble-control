"""Convex polygons."""
from dataclasses import dataclass
import random


@dataclass(frozen=True, eq=True)
class ConvexPolygon:
    """A convex polygon."""
    vertices: tuple[tuple[float, float], ...]
    elasticity: float
    friction: float

    @staticmethod
    def random(
            vertex_n: int,
            size_scale: float,
            position_scale: float,
            elasticity: float,
            friction: float,
            ) -> "ConvexPolygon":
        vertices = list()
        x = random.uniform(-position_scale/2, position_scale/2)
        y = random.uniform(-position_scale/2, position_scale/2)
        for _ in range(vertex_n):
            vertices.append((x, y))
            x += random.uniform(-size_scale/2, size_scale/2)
            y += random.uniform(-size_scale/2, size_scale/2)
        return ConvexPolygon(vertices, elasticity, friction)
