"""Throw a marble in a random board."""
from pathlib import Path
import random
from marble_control.board import Board
from marble_control.logging import images_to_video
from marble_control.logging import save_pixel_array


# Board parameters
POLYGON_N = 0
FORCE_RANGE = 1000
FORCE_FRAME_FREQUENCY = 60
WAYPOINT_N = 0

# Animation parameters
FPS = 60
STEPS = 600
VIDEO_PATH = Path("video.mp4")


def main():
    """Throw a marble in a random board and record a video."""
    fps = FPS
    steps = STEPS
    video_path = VIDEO_PATH
    if video_path.exists():
        video_path.unlink()
    filepaths = list()

    # Build board
    board = Board.random(polygon_n=POLYGON_N, waypoint_n=WAYPOINT_N)
    # Simulate board
    for i in range(steps):
        print(f"{i}/{steps}")
        if i % FORCE_FRAME_FREQUENCY == 0:
            board.apply_force_marble((
                random.uniform(-FORCE_RANGE, FORCE_RANGE),
                random.uniform(-FORCE_RANGE, FORCE_RANGE)
                ))
        board.step()
        filename = (video_path.parent/str(i).rjust(10)).with_suffix(".png")
        save_pixel_array(board.pixel_array, filename)
        filepaths.append(filename)

    # Save video and clean up
    images_to_video(video_path.parent, video_path, fps)
    for filename in filepaths:
        filename.unlink()


if __name__ == "__main__":
    main()
