"""Throw a marble in a random board."""
from pathlib import Path
import random
import json
from marble_control.board import Board
from marble_control.logging import images_to_video
from marble_control.logging import save_pixel_array


# Board parameters
POLYGON_N = 0
FORCE_RANGE = 1000
FORCE_FRAME_FREQUENCY = 60
WAYPOINT_N = 0

# Animation parameters
FPS = 60
STEPS = 600
VIDEO_PATH = Path("video.mp4")
SAVE_VIDEO = False
TABLE_PATH = Path("data.json")


def main():
    """Throw a marble in a random board and record a video."""
    fps = FPS
    steps = STEPS
    video_path = VIDEO_PATH
    if video_path.exists():
        video_path.unlink()
    filepaths = list()
    data = list()

    # Build board
    board = Board.random(polygon_n=POLYGON_N, waypoint_n=WAYPOINT_N)
    # Simulate board
    for i in range(steps):
        print(f"{i}/{steps}")
        state = (*board.marble.position, *board.marble.velocity)
        if i % FORCE_FRAME_FREQUENCY == 0:
            force = (
                random.uniform(-FORCE_RANGE, FORCE_RANGE),
                random.uniform(-FORCE_RANGE, FORCE_RANGE)
            )
        else:
            force = (0, 0)
        board.apply_force_marble(force)
        board.step()

        if SAVE_VIDEO:
            filename = (video_path.parent/str(i).rjust(10)).with_suffix(".png")
            save_pixel_array(board.pixel_array, filename)
            filepaths.append(filename)

        state_next = (*board.marble.position, *board.marble.velocity)
        data.append(dict(
            state=state,
            action=force,
            state_next=state_next,
        ))

    # Save video and clean up
    if SAVE_VIDEO:
        images_to_video(video_path.parent, video_path, fps)
        print(f"Saved {video_path}")
    for filename in filepaths:
        filename.unlink()

    # Save state evolution
    with open(TABLE_PATH, "wt") as f_p:
        f_p.write(json.dumps(data, indent=4))
    print(f"Saved {TABLE_PATH}")


if __name__ == "__main__":
    main()
